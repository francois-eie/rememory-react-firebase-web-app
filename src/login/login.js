import './login.scss';
import React, { Component } from 'react';
import globalValues from '../shared/global-values';
import { firebase } from '../shared/firebase';
import { appUrl } from '../shared/firebase'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // email: 'francoiskrugermail@gmail.com',
      // password: 'francois123',
      email: '',
      password: '',
      confirmPassword: '',
      showLogin: true
    }

    this.checkLoggedInUser();
  }

  checkLoggedInUser = () => {
    if (firebase.auth().currentUser) {
      // Logged in
      this.checkEmailVerified();
    } else {
      // Not logged in
      console.log('Not logged in already');
    }
  }

  checkEmailVerified = () => {
    const user = firebase.auth().currentUser;
    if (user.emailVerified) {
      globalValues.updateUserRecord();
      this.props.history.push('/home');
    } else {
      alert('Please complete the verification email we have sent you.');
      firebase.auth().currentUser.sendEmailVerification({url: `https://${appUrl}`});
    }
  }

  onClickLogin = () => {
    firebase
    .auth()
    .signInWithEmailAndPassword(this.state.email, this.state.password)
    .then(user => {
      this.checkEmailVerified();
    })
    .catch((error) => {
      alert('Error: onClickLogin. Check console');
      console.log(error);
    });
  }

  onClickSignUp = () => {
    if (this.state.password !== this.state.confirmPassword) {
      alert('Your passwords don\'t match');
      return;
    }

    firebase
    .auth()
    .createUserWithEmailAndPassword(this.state.email, this.state.password)
    .then(user => {
      alert('We have sent you a verification email. Follow it\'s instructions to continue.');
      firebase.auth().currentUser.sendEmailVerification({url: `https://${appUrl}`});
    })
    .catch((error) => {
      alert('Error: onClickLogin. Check console');
      console.log(error);
    });
  }

  onClickSwitch = () => {
    this.setState({
      showLogin: !this.state.showLogin
    });
  }

  render() {
    return (
      <div className="main">
        <div className="container">
          <label className="title">Rememory this screen? Good job</label>

          <div className="w100">
            <label className="lbl input">Email</label>
            <input className="inp" value={this.state.email} onChange={(e) => this.setState({ email: e.target.value })}></input>
          </div>

          <div className="w100">
            <label className="lbl input">Password</label>
            <input className="inp" type="password" value={this.state.password} onChange={(e) => this.setState({ password: e.target.value })}></input>
          </div>
          
          {/* Show login controls */}
          {this.state.showLogin && (
            <div>
              <br />
              <button className="btn primary" onClick={this.onClickLogin}>LOGIN</button>
            </div>
          )}

          {/* Show sign up controls */}
          {!this.state.showLogin && (
            <div style={{textAlign: "center"}} className="w100">
              <div className="w100">
                <label className="lbl input">Confirm Password</label>
                <input className="inp" type="password" value={this.state.confirmPassword} onChange={(e) => this.setState({ confirmPassword: e.target.value })}></input>
              </div>

              <br />
              <button className="btn primary" onClick={this.onClickSignUp}>SIGN UP</button>
            </div>
          )}

          {/* Switch login/sign up button */}
          <br />
          <button style={{marginTop: "0px"}} className="btn default no-border" onClick={this.onClickSwitch}>{this.state.showLogin ? 'Not registered? Sign Up here' : 'Already registered? Login here'}</button>
        </div>
      </div>
    );
  }

}

export default Login;