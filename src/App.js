import React from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom';
import Home from './home/home';
import Login from './login/login';

function App() {
  return (
    <BrowserRouter>
      <Switch>
        {/* Most complex at the top
        Least complex at the bottom */}
        <Route path="/home" component={Home}/>
        <Route path="/" component={Login}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
