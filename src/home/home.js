import './home.scss';
import React, { Component } from 'react';
import Calendar from 'react-calendar';
import uuidv4 from 'uuid/v4';
import { firebase } from '../shared/firebase';
import { requestNotificationPermission } from '../shared/firebase';
import usefulTools from '../shared/other/useful-tools';
const storageRef = firebase.storage().ref();

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // The selected date that you see (highlighted in blue)
      selectedDate: null,
      // The list of booked dates on the calendar
      bookedDates: [],
      // The title field
      title: '',
      // The photo input file picker field
      photoFilePicker: React.createRef(),
      // The photo img src display value
      photoDisplayUrl: '',
      // The description field
      description: '',
      // The is a crappy solution to clearing the input=file's file-text. Whenever this value changes, the input=file control is re-rendered.
      photoRefereshKey: Date.now(),
      // The selected memory (if the date has a memory attached to it)
      selectedMemory: null
    };
    // The original memories list from firebase
    this.memories = [];
    // Show loading animation
    this.showLoadingAnimation = false;


    this.getBookedDates();
    requestNotificationPermission();
  }

  getBookedDates() {
    // THIS FUNCTION GETS ALL THE MEMORIES CREATED BY THIS USER AND CREATES THE bookedDates ARRAY WHICH IS USED TO DISPLAY THE MEMORIES IN THE CALENDAR
    firebase
      .firestore()
      .collection('memories')
      .where('userEmail', '==', firebase.auth().currentUser.email)
      .onSnapshot((querySnapshot) => {
        this.memories = [];
        querySnapshot.forEach((element) => {
          this.memories.push({ ...element.data(), ...{ id: element.id } });
        });
        console.log('Memories: ', this.memories);

        this.setState({
          bookedDates: this.memories.map(element => usefulTools.dateToString(element.date.toDate())),
        });
      });
  }

  dayClicked = dateValue => {
    // THIS FUNCTION GETS THE DATA OF A MEMORY IF THE SELECTED DATE HAS A MEMORY ATTACHED TO IT

    // Check if the selected date is already booked
    const dateOnly = usefulTools.dateToString(dateValue);
    const memoryIndex = this.state.bookedDates.indexOf(dateOnly);

    if (memoryIndex > -1) {
      // If the date is already booked, get the booked information to display
      const tempSelectedMemory = this.memories[memoryIndex];

      // Download the photo file if there is one
      if (tempSelectedMemory.hasOwnProperty('photo')) {
        const photoRef = storageRef.child(tempSelectedMemory.photo);
        photoRef.getDownloadURL().then(photoDisplayUrl => {
          this.setState({ photoDisplayUrl })
        });
      }

      // Update all the fields
      this.setState({
        selectedDate: dateValue,
        title: tempSelectedMemory.title,
        description: tempSelectedMemory.description,
        selectedMemory: tempSelectedMemory,
        // This will be updated immediately after the getDownloadURL() above returns with a correct value. But we do this to clear it of previously selected photo URLs
        photoDisplayUrl: ''
      });
    } else {
      this.clearEverything(dateValue);
    }
  }

  clearEverything = (pDateValue) => {
    this.setState({
      title: '',
      description: '',
      selectedDate: pDateValue,
      photoDisplayUrl: '',
      // Here we update the photoRefereshKey, which forces a reload on the input=file control to update its display text
      photoRefereshKey: Date.now(),
      selectedMemory: null
    });
  }

  save = () => {
    // THIS FUNCTION CREATES/UPDATES A NEW MEMORY
    // FIRST IT BUILDS THE NEW MEMORY RECORD
    // THEN IT UPLOADS THE NEW PHOTO FILE
    // THEN IT CREATE THE MEMORY RECORD
    // (EACH IN IT'S OWN FUNCTION TO SIMPLIFY THE DEBUGGING)

    // Check if an item is selected
    if (!this.state.selectedDate) return;

    // Start loading animation
    if (this.showLoadingAnimation) return;
    this.showLoadingAnimation = true;

    // Create new/update record
    const recordData = {
      userEmail: firebase.auth().currentUser.email,
      date: this.state.selectedDate,
      title: this.state.title,
      description: this.state.description,
    };

    this.uploadPhoto(recordData);
  }

  uploadPhoto = (pMemoryRecord) => {
    // THIS FUNCTION UPLOADS A NEW PHOTO IF THE PHOTO IS NEW, IT ALSO DELETES THE PREVIOUS PHOTO FROM STORAGE IF IT NEEDS TO
    // THERE AFTER, IT CONTINUES ON TO THE createOrUpdateMemory() FUNCTION TO CREATE THE MEMORY RECORD

    const file = this.state.photoFilePicker.current.files[0];

    // Check if photo was removed
    let photoWasRemoved = false;
    if (this.state.selectedMemory !== null) {
      photoWasRemoved = (this.state.photoDisplayUrl === '' && this.state.selectedMemory.photo !== '' && this.state.selectedMemory.photo !== undefined);
    }

    // Check if photo was changed
    let photoWasChanged = false;
    let tempPhoto = this.state.selectedMemory ? this.state.selectedMemory.photo : undefined;
    if (file !== undefined && tempPhoto !== undefined && tempPhoto.indexOf('_') !== -1) {
      let previousFileName = tempPhoto.substring(tempPhoto.indexOf('_') + 1, tempPhoto.length);
      photoWasChanged = (file.name !== previousFileName);
    }

    if (photoWasRemoved || photoWasChanged) {
      // Delete the previous photo from storage
      this.deletePhotoFromStorage(false);
    }

    // Add the new file if there is one
    if (file !== undefined) {
      let fileName = uuidv4() + '_' + file.name;
      pMemoryRecord.photo = fileName;

      const photoRef = storageRef.child(fileName);
      photoRef
      .put(file)
      .then((snapshot) => {
        photoRef.getDownloadURL().then(photoDisplayUrl => {
          this.setState({ photoDisplayUrl })
        });
        this.createOrUpdateMemory(pMemoryRecord);
      }).catch((error) => {
        alert('File upload failed. Check console');
        console.log(error);
      });
    } else {
      this.createOrUpdateMemory(pMemoryRecord);
    }
  }

  createOrUpdateMemory = (pMemoryRecord) => {
    // THIS FUNCTION CREATES/UPDATES A MEMORY RECORD

    if (this.state.selectedMemory === null) {
      // CREATE a new record
      firebase
      .firestore()
      .collection('memories')
      .add(pMemoryRecord)
      .then(newDocumentId => {
        this.setState({
          selectedMemory: this.memories.filter(element => element.id === newDocumentId.id)[0]
        });
      }).catch((error) => {
        alert('Error saving data. Check console');
        console.log(error);
      }).finally(() => {
        this.showLoadingAnimation = false;
      });
    } else {
      // UPDATE a record
      firebase
      .firestore()
      .collection('memories')
      .doc(this.state.selectedMemory.id)
      .set(pMemoryRecord)
      .then(() => {
        this.setState({
          selectedMemory: this.memories.filter(element => element.id === this.state.selectedMemory.id)[0]
        });
        this.showLoadingAnimation = false;
      }).catch((error) => {
        alert('Error updating data. Check console');
        console.log(error);
      }).finally(() => {
        this.showLoadingAnimation = false;
      });
    }
  }

  removePhoto = () => {
    this.setState({
      photoDisplayUrl: '',
      // The is a crappy solution to clearing the input=file's file-text. Whenever this value changes, the input=file control is re-rendered.
      photoRefereshKey: Date.now()
    });
  }

  delete = async () => {
    // THIS FUNCTION DELETES THE SELECTED MEMORY & PHOTO FILE IN THE STORAGE

    if (!window.confirm('This selected memory will now be deleted')) return;

    // Delete the selected record from firebase
    await firebase
    .firestore()
    .collection('memories')
    .doc(this.state.selectedMemory.id)
    .delete()
    .then(() => {

    }).catch((error) => {
      alert('Delete failed, Check console');
      console.log(error);
    });

    this.deletePhotoFromStorage(true);
  }

  deletePhotoFromStorage = (pClearEverything) => {
    // THIS FUNCTION DELETES THE SELECTED MEMORY PHOTO FROM FIREBASE STORAGE
    storageRef
    .child(this.state.selectedMemory.photo)
    .delete()
    .then((e) => {
      if (pClearEverything) this.clearEverything();
    }).catch((error) => {
      alert('Storage file delete failed. Check console');
      console.log(error);
    });
  }

  buildControls = () => {
    if (!this.state.selectedDate) return;
    
    return (
      <div>
        <label className="lbl input">Title</label>
        <input className="inp" value={this.state.title} onChange={(e) => this.setState({ title: e.target.value })}></input>
        <br />

        <label className="lbl input">Description</label>
        <textarea className="inp" value={this.state.description} onChange={(e) => this.setState({ description: e.target.value })}></textarea>
        <br />

        <label className="lbl input">Photo</label>
        <input className="inp" accept="image/*" type="file" ref={this.state.photoFilePicker} onChange={(e) => this.setState({ photoDisplayUrl: URL.createObjectURL(e.target.files[0]) })} key={this.state.photoRefereshKey}></input>
        <br />

        {this.state.photoDisplayUrl.length > 0 && (
          <div>
            <img src={this.state.photoDisplayUrl} alt="" height="300" width="400" />
            <button onClick={this.removePhoto}>Remove Photo</button>
          </div>
        )}
        <br />

        <div style={{textAlign: "center"}}>
          <button className="btn primary" onClick={this.save}>Save</button>
          {this.state.selectedMemory !== null && (<button className="btn default" onClick={this.delete}>Delete</button>)}
        </div>
      </div>
    )
  }

  render() {
    return (
      <div className="main">
        <div className="container">
          {!this.state.selectedDate && (
            <div style={{marginBottom: '50px'}}>
              <label>Come on, just click something</label>
            </div>
          )}

          <div>
          <Calendar
            onClickDay={this.dayClicked}
            value={this.state.selectedDate}
            bookedDates={this.state.bookedDates}
            style={{marginBottom: '20px'}}
          />
          </div>

          {this.buildControls()}
        </div>
      </div>
    )
  }
}

export default Home;