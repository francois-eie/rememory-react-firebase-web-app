import { firebase } from './firebase';
import models from './models';

function updateUserRecord() {
    const email = firebase.auth().currentUser.email || '';
    if (!(email !== '' && email !== null && email !== undefined)) return;

    const user = new models.User(email, sessionStorage.getItem('Rememory_notificationToken'));

    firebase
    .firestore()
    .collection('users')
    .doc(email)
    .set(user.toObject())
    .then(() => {

    }).catch((error) => {
        alert('Update user failed. Check console');
        console.log(error);
    });
}

export default {
    updateUserRecord
};