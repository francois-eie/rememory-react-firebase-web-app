class User {
  constructor(pEmail, pNotificationToken) {
    this.email = pEmail;
    this.notificationToken = pNotificationToken;
  }

  toObject = () => {
    return {
      email: this.email,
      notificationToken: this.notificationToken,
    }
  }
};

class Memory {
  constructor(pUserEmail, pDate, pTitle, pDescription, pPhoto) {
    this.userEmail = pUserEmail;
    this.date = pDate;
    this.title = pTitle;
    this.description = pDescription;
    this.photo = pPhoto;
  }

  toObject = () => {
    return {
      userEmail: this.userEmail,
      date: this.date,
      title: this.title,
      description: this.description,
      photo: this.photo,
    }
  }
};

export default {
  User,
  Memory
};