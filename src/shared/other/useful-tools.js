
function dateToString(pDate) {
    // Takes a date object (new Date()) and returns it as YYYY-MM-DD
    const getDate = pDate.getDate();
    const day = getDate < 10 ? '0' + getDate : getDate;
    const month = pDate.getMonth() + 1;
    const year = pDate.getFullYear();
    return `${year}-${month}-${day}`;
}

module.exports = {
    dateToString
}
