import globalValues from './global-values';
import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/storage';
import 'firebase/messaging';
import 'firebase/firestore';

// DEV
// const firebaseConfig = {
//   environment: 'DEV',
//   apiKey: "AIzaSyBxyc0I02B_7HEJDtqpKHXZDaNe2iYx2AE",
//   authDomain: "rememory-dev.firebaseapp.com",
//   databaseURL: "https://rememory-dev.firebaseio.com",
//   projectId: "rememory-dev",
//   storageBucket: "rememory-dev.appspot.com",
//   messagingSenderId: "403877010912", // Used for firebase messaging in the public/firebase-messaging-sq.js file
//   appId: "1:403877010912:web:44b2729fe969d4fea99f85",
//   measurementId: "G-S2143TESME",
//   publicVapidKey: "BDN-ZVi3ZblMBCKX6iCLQdcM4RTyytdUXQ6M0f-2-S1fRiRZhWE7H9GmxpAOGIjjr9d9FgSPzIyGBwfFG-n_2Ho"
// }

// PRD
const firebaseConfig = {
  environment: 'PRD',
  apiKey: "AIzaSyABVjw5K6qBt8oroGy6zMVKO1VXUr2edTc",
  authDomain: "rememory-prd.firebaseapp.com",
  databaseURL: "https://rememory-prd.firebaseio.com",
  projectId: "rememory-prd",
  storageBucket: "rememory-prd.appspot.com",
  messagingSenderId: "594823373575", // Used for firebase messaging in the public/firebase-messaging-sq.js file
  appId: "1:594823373575:web:399b9a053be8bfb61f5ba2",
  measurementId: "G-7T20TXLNX2",
  publicVapidKey: "BP-gLz_qgurb9rMjqo3QuYf8LQhoqXa0YSIqvW3maVdWv6RNTRVFXqRf4d39PmyKDqhVj8lrNGq26xbl5Wo5h5w"
}

// This is used for the firebase sendEmailVerification() to route back to the app
// We don't access the firebaseConfig directly from other files as it contains secure information
const appUrl = firebaseConfig.authDomain;

console.log(`Version 1.1.1 ${firebaseConfig.environment}`);

// Intialize the firebase module
const initializedFirebaseApp = firebase.initializeApp(firebaseConfig);
const messaging = initializedFirebaseApp.messaging();
// Generate a new key pair
// Open the Cloud Messaging tab of the Firebase console Settings pane and scroll to the Web configuration section.
// In the Web Push certificates tab, click Generate Key Pair. The console displays a notice that the key pair was generated, and displays the public key string and date added.
messaging.usePublicVapidKey(firebaseConfig.publicVapidKey);

const requestNotificationPermission = function() {
  // alert('Rememory will now ask you for Notification permissions. This is for us to send you a memory notification while you are not on the website :)');

  // Request notification access
  messaging.requestPermission()
  .then(async () => {
      const token = await messaging.getToken();
      sessionStorage.setItem('Rememory_notificationToken', token);
      globalValues.updateUserRecord();
  })
  .catch(function (err) {
    console.log("Unable to get permission to notify.", err);
  });
  
  navigator.serviceWorker.addEventListener("message", (message) => console.log(message));
}

export {
  firebase,
  requestNotificationPermission,
  appUrl
};