// To check logs run: firebase functions:log
const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.sendNotifications = functions.https.onRequest((request, response) => {
  // Basic password check
  if (!(request.query.hasOwnProperty('lawdHoSum') && request.query.lawdHoSum === 'sendThisAlreadyDude')) return response.send('Invalid login details');
  
  // Get all the users
  admin.firestore().collection('users').get().then(querySnapshot => {
    let arrUsers = querySnapshot.docs.map(element => element.data());
    arrUsers = arrUsers.filter(element => element.notificationToken !== null && element.notificationToken !== '' && element.notificationToken !== undefined)

    // Send a message to each user's notificationToken
    arrUsers.forEach(element => {

      // The actual message that is sent
      return admin.messaging().sendToDevice(element.notificationToken, {
        notification: {
          title: 'You have been invited to a trip.',
          body: 'Tap here to check it out!'
        }
      });

    });

    return response.send('Notifications sent!');
  }).catch((error) => {
    return response.status(500).send(error);
  });
});
